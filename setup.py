from setuptools import setup, find_packages

setup(
    name="awesomemultiplication",
    version="101",
    python_requires=">=3.9",
    description="best muliplication package available",
    author="Jeroen Groothedde",
    author_email="j.groothedde@cmotions.nl",
    install_requires=[
        "pandas>=1.1.5",
        "pyodbc>=4.0.30"
    ],
    extras_require={ 
        "dev":[
            "black",
            "jupyterlab"
        ],
    }
)