"""this module tests the methods of the mathfunction in the module functionfile.py"""

import pytest
from multiply import functionfile as ff

@pytest.mark.parametrize("nr1, nr2, expected", [(1,2,2),(2,3,6),(6,10,60)])
def test_multiply_numbers(nr1, nr2, expected):
    """"
    tests
    """
    result = ff.simplefunction(x = nr1, y= nr2)
    assert result == expected
