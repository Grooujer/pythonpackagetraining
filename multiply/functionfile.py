from numbers import Number

def simplefunction(x: Number , y: Number) -> Number:
    """
    This function multiplies x and y and returns the outcome

    Args:
        x (Number): first number to multiply with
        y (Number): float to muliply the first number with

    Returns:
        Number: the outcome of the muliplication of the input x and y

    """
    return x * y